﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeneradoraDeDatos
{
    /// <summary>
    /// Genera una lista de puntos (X,Y) en base a una función matemática proporcionada
    /// </summary>
    public class Generadora
    {
        public Generadora()
        {
            Puntos = new List<Punto>();
        }
        /// <summary>
        /// Permite obtener el ultimo conjunto de datos generado
        /// </summary>
        public List<Punto> Puntos { get; set; }
        /// <summary>
        /// Genera un conjunto de datos tipo Punto 
        /// </summary>
        /// <param name="limiteInferior">Limite inferior de X para generar los datos</param>
        /// <param name="limiteSuperior">Limite superior de X para generar los datos</param>
        /// <param name="incremento">Incremento de X para generar los datos</param>
        /// <returns></returns>
        public List<Punto> GenerarDatos(double limiteInferior, double limiteSuperior, double incremento)
        {
            Puntos = new List<Punto>();
            for (double x = limiteInferior; x < limiteSuperior; x+=incremento)
            {
                Puntos.Add(new Punto(x,Evaluar(x)));
            }
            return Puntos;
        }

        private double Evaluar(double x)
        {
            //Aqui cambiar la función para evaluar
            return Math.Pow(x, 2);
        }
    }
}
