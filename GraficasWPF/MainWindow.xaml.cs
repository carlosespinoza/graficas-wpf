﻿using GeneradoraDeDatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Axes;

namespace GraficasWPF
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Generadora generador;
        Random r = new Random();
        public MainWindow()
        {
            InitializeComponent();
            btnCalcular.Click += BtnCalcular_Click;
            generador = new Generadora();
        }

        private void BtnCalcular_Click(object sender, RoutedEventArgs e)
        {
            generador.GenerarDatos(float.Parse(txbLimInferor.Text), float.Parse(txbLimSuperior.Text), float.Parse(txbIncremento.Text));
            Tabla.ItemsSource = null;
            Tabla.ItemsSource = generador.Puntos;
            PlotModel model = new PlotModel();
            LinearAxis ejeX = new LinearAxis();
            ejeX.Minimum = double.Parse(txbLimInferor.Text);
            ejeX.Maximum = double.Parse(txbLimSuperior.Text);
            ejeX.Position = AxisPosition.Bottom;

            LinearAxis ejeY = new LinearAxis();
            ejeY.Minimum = generador.Puntos.Min(p => p.Y);
            ejeY.Maximum = generador.Puntos.Max(p => p.Y);
            ejeY.Position = AxisPosition.Left;
            
            model.Axes.Add(ejeX);
            model.Axes.Add(ejeY);
            model.Title = "Datos generados";
            LineSeries linea = new LineSeries();
            foreach (var item in generador.Puntos)
            {
                linea.Points.Add(new DataPoint(item.X, item.Y));
            }
            linea.Title = "Valores generados";
            linea.Color = OxyColor.FromRgb(byte.Parse(r.Next(0,255).ToString()), byte.Parse(r.Next(0, 255).ToString()), byte.Parse(r.Next(0, 255).ToString()));
            model.Series.Add(linea);
            Grafica.Model = model;
           
        }
    }
}
